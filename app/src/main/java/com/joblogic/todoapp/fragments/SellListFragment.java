package com.joblogic.todoapp.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.joblogic.todoapp.R;
import com.joblogic.todoapp.adapters.ToSellAdapter;
import com.joblogic.todoapp.databases.RoomRepo;
import com.joblogic.todoapp.databinding.FragmentToSellBinding;
import com.joblogic.todoapp.interfaces.MainNavigationDelegate;
import com.joblogic.todoapp.models.ItemToSell;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class SellListFragment extends BaseFragment {

    private ArrayList<ItemToSell> sellsList;
    private ToSellAdapter sellAdapter;

    private FragmentToSellBinding binding;

    public SellListFragment() {
    }

    public static SellListFragment newInstance(Activity activity, MainNavigationDelegate mainNavigationDelegate) {
        SellListFragment fragment = new SellListFragment();
        fragment.setBase(activity, mainNavigationDelegate, R.string.to_sell);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentToSellBinding.inflate(inflater, container, false);

        setupViews();
        getSellListFromDatabase();

        return binding.getRoot();
    }

    private void setupViews() {
        sellsList = new ArrayList<>();
        sellAdapter = new ToSellAdapter(context, sellsList);
        binding.recyclerViewToSell.setAdapter(sellAdapter);
        binding.recyclerViewToSell.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    }

    private void getSellListFromDatabase() {
        showLoading();
        AsyncTask.execute(() -> {
            List<ItemToSell> itemsToSell = RoomRepo.getInstance(context).getAllItemsToSell();
            activity.runOnUiThread(() -> updateList(itemsToSell));
        });

    }

    private void updateList(List<ItemToSell> sellList) {
        hideLoading();
        sellsList.clear();
        sellsList.addAll(sellList);
        sellAdapter.notifyDataSetChanged();
    }

    private void showLoading() {
        binding.viewContainerLoader.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        binding.viewContainerLoader.setVisibility(View.GONE);
    }

}
