package com.joblogic.todoapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

import com.joblogic.todoapp.interfaces.MainNavigationDelegate;
import com.joblogic.todoapp.utils.Consts;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public abstract class BaseFragment extends Fragment {

    protected Activity activity;
    protected Context context;
    protected MainNavigationDelegate mainNavigationDelegate;
    private String title;

    public BaseFragment() {
    }

    public void setBase(@NotNull Activity activity, @NotNull MainNavigationDelegate mainNavigationDelegate, String title) {
        this.activity = activity;
        this.context = activity;
        this.mainNavigationDelegate = mainNavigationDelegate;
        getArg().putString(Consts.EXTRA_TITLE, title);
    }

    public void setBase(@NotNull Activity activity, @NotNull MainNavigationDelegate mainNavigationDelegate, @StringRes int titleRes) {
        this.activity = activity;
        this.context = activity;
        this.mainNavigationDelegate = mainNavigationDelegate;
        getArg().putString(Consts.EXTRA_TITLE, activity.getString(titleRes));
    }

    public void setTitle(String title) {
        this.title = title;
        getArg().putString(Consts.EXTRA_TITLE, title);
        activity.setTitle(title);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getArg().getString(Consts.EXTRA_TITLE);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof Activity) {
            this.activity = (Activity) context;
        }
        if (context instanceof MainNavigationDelegate) {
            this.mainNavigationDelegate = (MainNavigationDelegate) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (title == null || title.isEmpty()) {
            return;
        }
        mainNavigationDelegate.setActivityTitle(title);
    }

    @NotNull
    public Bundle getArg() {
        Bundle arguments = getArguments();
        if (arguments == null) {
            arguments = new Bundle();
            setArguments(arguments);
        }
        return arguments;
    }

}
