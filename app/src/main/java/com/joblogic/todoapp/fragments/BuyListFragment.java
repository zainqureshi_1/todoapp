package com.joblogic.todoapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.joblogic.todoapp.R;
import com.joblogic.todoapp.adapters.ToBuyAdapter;
import com.joblogic.todoapp.apis.ApiInstance;
import com.joblogic.todoapp.databinding.FragmentToBuyBinding;
import com.joblogic.todoapp.interfaces.MainNavigationDelegate;
import com.joblogic.todoapp.models.ItemToBuy;
import com.joblogic.todoapp.utils.LogUtil;
import com.joblogic.todoapp.utils.Utility;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class BuyListFragment extends BaseFragment {
    private final String TAG = "BuyListFragment";

    private ArrayList<ItemToBuy> buysList;
    private ToBuyAdapter buyAdapter;

    private FragmentToBuyBinding binding;

    public BuyListFragment() {
    }

    public static BuyListFragment newInstance(Activity activity, MainNavigationDelegate mainNavigationDelegate) {
        BuyListFragment fragment = new BuyListFragment();
        fragment.setBase(activity, mainNavigationDelegate, R.string.to_buy);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentToBuyBinding.inflate(inflater, container, false);

        setupViews();
        callApi();

        return binding.getRoot();
    }

    private void setupViews() {
        buysList = new ArrayList<>();
        buyAdapter = new ToBuyAdapter(context, buysList);
        binding.recyclerViewToBuy.setAdapter(buyAdapter);
        binding.recyclerViewToBuy.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    }

    private void callApi() {
        showLoading();
        ApiInstance.getAPIService().getToBuyList().enqueue(buyListCallback);
    }
    
    private void updateList(List<ItemToBuy> buyList) {
        buysList.clear();
        buysList.addAll(buyList);
        buyAdapter.notifyDataSetChanged();
    }

    private void showError(String error) {
        LogUtil.e(TAG, error);
        hideLoading();
        if (error == null || error.isEmpty()) {
            error = context.getString(R.string.unknown_error_occurred_please_try_again);
        }
        Utility.showToast(context, error);
    }

    private void showLoading() {
        binding.viewContainerLoader.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        binding.viewContainerLoader.setVisibility(View.GONE);
    }

    private Callback<List<ItemToBuy>> buyListCallback = new Callback<List<ItemToBuy>>() {
        @Override
        public void onResponse(@NonNull Call<List<ItemToBuy>> buy, @NonNull Response<List<ItemToBuy>> response) {
            hideLoading();
            if (!response.isSuccessful()) {
                ResponseBody errorBody = response.errorBody();
                if (errorBody != null) {
                    try {
                        String stringError = errorBody.string();
                        try {
                            JSONObject jsonResult = new JSONObject(stringError);
                            if (jsonResult.has("message")) {
                                String message = jsonResult.getString("message");
                                showError(message);
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showError(stringError);
                        return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                showError("Api response was not successful. Please try again.");
                return;
            }
            List<ItemToBuy> buyList = response.body();
            if (buyList == null) {
                showError("Api response body is null. Please try again.");
                return;
            }
            updateList(buyList);
        }
        @Override
        public void onFailure(@NonNull Call<List<ItemToBuy>> buy, @NonNull Throwable t) {
            hideLoading();
            LogUtil.e(TAG, t.getLocalizedMessage(), t);
            showError(t.getLocalizedMessage());
        }
    };

}
