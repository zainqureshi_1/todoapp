package com.joblogic.todoapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;

import com.joblogic.todoapp.R;
import com.joblogic.todoapp.databinding.FragmentHomeBinding;
import com.joblogic.todoapp.interfaces.MainNavigationDelegate;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class HomeFragment extends BaseFragment {

    private FragmentHomeBinding binding;

    public HomeFragment() {
    }

    public static HomeFragment newInstance(Activity activity, MainNavigationDelegate mainNavigationDelegate) {
        HomeFragment fragment = new HomeFragment();
        fragment.setBase(activity, mainNavigationDelegate, R.string.app_name);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);

        setupViews();

        return binding.getRoot();
    }

    private void setupViews() {
        binding.buttonToCall.setOnClickListener(v -> buttonClicked(v.getId()));
    }

    private void buttonClicked(@IdRes int id) {
        switch (id) {
            case R.id.buttonToCall:
                mainNavigationDelegate.gotoCallListScreen();
                break;
            case R.id.buttonToBuy:
                mainNavigationDelegate.gotoBuyListScreen();
                break;
            case R.id.buttonToSell:
                mainNavigationDelegate.gotoSellListScreen();
                break;
            default:
                break;
        }
    }

}
