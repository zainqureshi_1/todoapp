package com.joblogic.todoapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.joblogic.todoapp.R;
import com.joblogic.todoapp.adapters.ToCallAdapter;
import com.joblogic.todoapp.apis.ApiInstance;
import com.joblogic.todoapp.databinding.FragmentToCallBinding;
import com.joblogic.todoapp.interfaces.MainNavigationDelegate;
import com.joblogic.todoapp.models.ToCall;
import com.joblogic.todoapp.utils.LogUtil;
import com.joblogic.todoapp.utils.Utility;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class CallListFragment extends BaseFragment {
    private final String TAG = "CallListFragment";

    private ArrayList<ToCall> callsList;
    private ToCallAdapter callAdapter;

    private FragmentToCallBinding binding;

    public CallListFragment() {
    }

    public static CallListFragment newInstance(Activity activity, MainNavigationDelegate mainNavigationDelegate) {
        CallListFragment fragment = new CallListFragment();
        fragment.setBase(activity, mainNavigationDelegate, R.string.to_call);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentToCallBinding.inflate(inflater, container, false);

        setupViews();
        callApi();

        return binding.getRoot();
    }

    private void setupViews() {
        callsList = new ArrayList<>();
        callAdapter = new ToCallAdapter(context, callsList);
        binding.recyclerViewToCall.setAdapter(callAdapter);
        binding.recyclerViewToCall.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    }

    private void callApi() {
        showLoading();
        ApiInstance.getAPIService().getToCallList().enqueue(callListCallback);
    }

    private void updateList(List<ToCall> callList) {
        callsList.clear();
        callsList.addAll(callList);
        callAdapter.notifyDataSetChanged();
    }

    private void showError(String error) {
        LogUtil.e(TAG, error);
        hideLoading();
        if (error == null || error.isEmpty()) {
            error = context.getString(R.string.unknown_error_occurred_please_try_again);
        }
        Utility.showToast(context, error);
    }

    private void showLoading() {
        binding.viewContainerLoader.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        binding.viewContainerLoader.setVisibility(View.GONE);
    }

    private Callback<List<ToCall>> callListCallback = new Callback<List<ToCall>>() {
        @Override
        public void onResponse(@NonNull Call<List<ToCall>> call, @NonNull Response<List<ToCall>> response) {
            hideLoading();
            if (!response.isSuccessful()) {
                ResponseBody errorBody = response.errorBody();
                if (errorBody != null) {
                    try {
                        String stringError = errorBody.string();
                        try {
                            JSONObject jsonResult = new JSONObject(stringError);
                            if (jsonResult.has("message")) {
                                String message = jsonResult.getString("message");
                                showError(message);
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showError(stringError);
                        return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                showError("Api response was not successful. Please try again.");
                return;
            }
            List<ToCall> callList = response.body();
            if (callList == null) {
                showError("Api response body is null. Please try again.");
                return;
            }
            updateList(callList);
        }
        @Override
        public void onFailure(@NonNull Call<List<ToCall>> call, @NonNull Throwable t) {
            hideLoading();
            LogUtil.e(TAG, t.getLocalizedMessage(), t);
            showError(t.getLocalizedMessage());
        }
    };

}
