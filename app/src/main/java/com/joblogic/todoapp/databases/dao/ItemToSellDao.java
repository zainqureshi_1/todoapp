package com.joblogic.todoapp.databases.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.joblogic.todoapp.models.ItemToSell;

import java.util.List;

/**
 *
 * Created by Zain on 9/8/2020.
 */

@Dao
public interface ItemToSellDao {

    @Query("SELECT * FROM ItemToSell")
    List<ItemToSell> getAll();

}
