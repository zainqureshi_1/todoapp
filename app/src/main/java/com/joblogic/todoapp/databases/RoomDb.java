package com.joblogic.todoapp.databases;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.joblogic.todoapp.databases.dao.ItemToSellDao;
import com.joblogic.todoapp.models.ItemToSell;
import com.joblogic.todoapp.sqlAsset.AssetSQLiteOpenHelperFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * Created by Zain on 9/8/2020.
 */

@Database(entities = {ItemToSell.class}, version = 1, exportSchema = false)
public abstract class RoomDb extends RoomDatabase {

    private static volatile RoomDb INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static RoomDb getDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (RoomDb.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDb.class, "jl-demo-db.db")
                            .openHelperFactory(new AssetSQLiteOpenHelperFactory())
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    abstract ItemToSellDao itemToSellDao();

}
