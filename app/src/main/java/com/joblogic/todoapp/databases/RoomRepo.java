package com.joblogic.todoapp.databases;

import android.content.Context;

import com.joblogic.todoapp.databases.dao.ItemToSellDao;
import com.joblogic.todoapp.models.ItemToSell;

import java.util.List;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class RoomRepo {

    private ItemToSellDao itemToSellDao;

    private static RoomRepo instance;

    public static RoomRepo getInstance(Context context) {
        if (instance == null) {
            instance = new RoomRepo(context);
        }
        return instance;
    }

    private RoomRepo(Context context) {
        RoomDb roomDb = RoomDb.getDatabase(context);
        itemToSellDao = roomDb.itemToSellDao();
    }

    public List<ItemToSell> getAllItemsToSell() {
        return itemToSellDao.getAll();
    }

}
