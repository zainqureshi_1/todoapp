package com.joblogic.todoapp.apis;

import com.joblogic.todoapp.models.ItemToBuy;
import com.joblogic.todoapp.models.ToCall;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public interface ApiInterface {

    @GET("call")
    @Headers({"Content-type: application/json"})
    Call<List<ToCall>> getToCallList();

    @GET("buy")
    @Headers({"Content-type: application/json"})
    Call<List<ItemToBuy>> getToBuyList();

}
