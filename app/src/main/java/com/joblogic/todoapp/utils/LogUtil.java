package com.joblogic.todoapp.utils;

import android.util.Log;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class LogUtil {

    private static final boolean LOG_ENABLED = true;

    public static void i(String tag, String msg) {
        if (!LOG_ENABLED) {
            return;
        }
        Log.i(tag, msg);
    }

    public static void e(String tag, String msg) {
        if (!LOG_ENABLED) {
            return;
        }
        Log.e(tag, msg);
    }

    public static void e(String tag, String msg, Throwable t) {
        if (!LOG_ENABLED) {
            return;
        }
        Log.e(tag, msg, t);
    }

}
