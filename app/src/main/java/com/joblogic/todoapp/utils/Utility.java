package com.joblogic.todoapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import androidx.annotation.StringRes;

import com.joblogic.todoapp.R;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class Utility {

    public static boolean isInternetConnected(Context context, boolean showErrorToast) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        if (!isConnected && showErrorToast) {
            showToast(context, R.string.connect_to_internet_and_try_again);
        }
        return isConnected;
    }

    public static void showToast(Context context, @StringRes int stringRes) {
        showToast(context, context.getString(stringRes));
    }

    public static void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

}
