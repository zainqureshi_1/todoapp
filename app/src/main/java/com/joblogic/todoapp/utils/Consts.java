package com.joblogic.todoapp.utils;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class Consts {

    public static final String EXTRA_TITLE = "EXTRA_TITLE";

    public static final String API_SERVER_URL = "https://my-json-server.typicode.com/imkhan334/demo-1/";

}
