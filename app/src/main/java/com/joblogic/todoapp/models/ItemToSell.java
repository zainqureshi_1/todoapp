package com.joblogic.todoapp.models;

import android.content.Context;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.joblogic.todoapp.R;

/**
 *
 * Created by Zain on 9/8/2020.
 */

@Entity
public class ItemToSell {

    @PrimaryKey
    private final int id;
    private final String name;
    private final int price;
    private final int quantity;
    private final int type;

    public ItemToSell(int id, String name, int price, int quantity, int type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getType() {
        return type;
    }

    public String getTypeString(Context context) {
        return context.getString(type == 1 ? R.string.to_buy : R.string.to_sell);
    }

}
