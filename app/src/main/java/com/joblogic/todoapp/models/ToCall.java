package com.joblogic.todoapp.models;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class ToCall {

    private final int id;
    private final String name;
    private final String number;

    public ToCall(int id, String name, String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

}
