package com.joblogic.todoapp.activities;

import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.os.Handler;

import com.joblogic.todoapp.R;
import com.joblogic.todoapp.databinding.ActivityMainBinding;
import com.joblogic.todoapp.fragments.BuyListFragment;
import com.joblogic.todoapp.fragments.CallListFragment;
import com.joblogic.todoapp.fragments.HomeFragment;
import com.joblogic.todoapp.fragments.SellListFragment;
import com.joblogic.todoapp.interfaces.MainNavigationDelegate;
import com.joblogic.todoapp.utils.Utility;

public class MainActivity extends AppCompatActivity implements MainNavigationDelegate {


    private ActionBar actionBar;

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setSupportActionBar(binding.toolbar);
        actionBar = getSupportActionBar();

        gotoHomeScreen();
    }

    private void showUpButton(boolean show) {
        actionBar.setDisplayHomeAsUpEnabled(show);
    }

    @Override
    public void setActivityTitle(@StringRes int resId) {
        setTitle(resId);
    }

    @Override
    public void setActivityTitle(String title) {
        setTitle(title);
    }

    @Override
    public void gotoHomeScreen() {
        HomeFragment fragment = HomeFragment.newInstance(this, this);
        gotoFragment(fragment);
    }

    @Override
    public void gotoCallListScreen() {
        CallListFragment fragment = CallListFragment.newInstance(this, this);
        gotoChildFragment(fragment);
    }

    @Override
    public void gotoBuyListScreen() {
        BuyListFragment fragment = BuyListFragment.newInstance(this, this);
        gotoChildFragment(fragment);
    }

    @Override
    public void gotoSellListScreen() {
        SellListFragment fragment = SellListFragment.newInstance(this, this);
        gotoChildFragment(fragment);
    }

    @Override
    public void gotoFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayoutFragments, fragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void gotoChildFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.replace(R.id.frameLayoutFragments, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
        showUpButton(true);
    }

    @Override
    public boolean popBackStack() {
        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackCount > 0) {
            getSupportFragmentManager().popBackStack();
            if (backStackCount == 1) {
                showUpButton(false);
            }
            return true;
        }
        return false;
    }

    private boolean backPressed = false;

    @Override
    public void onBackPressed() {
        if (popBackStack()) {
            return;
        }
        if (backPressed) {
            /*if (getCurrentFragment() instanceof MarshalFlightsFragment) {
                FirebaseManager.getInstance().signOutUser();
            }*/
            super.onBackPressed();
            return;
        }

        backPressed = true;
        Utility.showToast(this, R.string.press_back_again_to_exit);

        new Handler().postDelayed(() -> backPressed = false, 2000);
    }

}