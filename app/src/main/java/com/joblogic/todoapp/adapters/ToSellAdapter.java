package com.joblogic.todoapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.joblogic.todoapp.databinding.CellToSellBinding;
import com.joblogic.todoapp.models.ItemToSell;

import java.util.ArrayList;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class ToSellAdapter extends RecyclerView.Adapter<ToSellAdapter.SellViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ItemToSell> sellsList;

    public ToSellAdapter(Context context, ArrayList<ItemToSell> sellsList) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.sellsList = sellsList;
    }

    @Override
    public int getItemCount() {
        return sellsList.size();
    }

    @NonNull
    @Override
    public SellViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CellToSellBinding binding = CellToSellBinding.inflate(inflater, parent, false);
        return new SellViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SellViewHolder holder, int position) {
        holder.bind(sellsList.get(position));
    }

    static class SellViewHolder extends RecyclerView.ViewHolder {

        private CellToSellBinding binding;

        SellViewHolder(CellToSellBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ItemToSell itemToSell) {
            binding.setSell(itemToSell);
            binding.executePendingBindings();
        }

    }

}
