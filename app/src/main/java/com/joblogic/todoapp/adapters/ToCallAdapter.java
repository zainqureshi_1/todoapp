package com.joblogic.todoapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.joblogic.todoapp.databinding.CellToCallBinding;
import com.joblogic.todoapp.models.ToCall;

import java.util.ArrayList;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class ToCallAdapter extends RecyclerView.Adapter<ToCallAdapter.CallViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ToCall> callsList;

    public ToCallAdapter(Context context, ArrayList<ToCall> callsList) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.callsList = callsList;
    }

    @Override
    public int getItemCount() {
        return callsList.size();
    }

    @NonNull
    @Override
    public CallViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CellToCallBinding binding = CellToCallBinding.inflate(inflater, parent, false);
        return new CallViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CallViewHolder holder, int position) {
        holder.bind(callsList.get(position));
    }

    static class CallViewHolder extends RecyclerView.ViewHolder {

        private CellToCallBinding binding;

        CallViewHolder(CellToCallBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ToCall toCall) {
            binding.setCall(toCall);
            binding.executePendingBindings();
        }

    }

}
