package com.joblogic.todoapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.joblogic.todoapp.databinding.CellToBuyBinding;
import com.joblogic.todoapp.models.ItemToBuy;

import java.util.ArrayList;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public class ToBuyAdapter extends RecyclerView.Adapter<ToBuyAdapter.BuyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ItemToBuy> buysList;

    public ToBuyAdapter(Context context, ArrayList<ItemToBuy> buysList) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.buysList = buysList;
    }

    @Override
    public int getItemCount() {
        return buysList.size();
    }

    @NonNull
    @Override
    public BuyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CellToBuyBinding binding = CellToBuyBinding.inflate(inflater, parent, false);
        return new BuyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BuyViewHolder holder, int position) {
        holder.bind(buysList.get(position));
    }

    static class BuyViewHolder extends RecyclerView.ViewHolder {

        private CellToBuyBinding binding;

        BuyViewHolder(CellToBuyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ItemToBuy itemToBuy) {
            binding.setBuy(itemToBuy);
            binding.executePendingBindings();
        }

    }

}
