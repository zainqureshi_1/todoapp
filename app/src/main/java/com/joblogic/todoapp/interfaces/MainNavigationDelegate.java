package com.joblogic.todoapp.interfaces;

import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

/**
 *
 * Created by Zain on 9/8/2020.
 */

public interface MainNavigationDelegate {
    void setActivityTitle(@StringRes int resId);
    void setActivityTitle(String title);

    void gotoHomeScreen();
    void gotoCallListScreen();
    void gotoBuyListScreen();
    void gotoSellListScreen();

    void gotoFragment(Fragment fragment);
    void gotoChildFragment(Fragment fragment);
    boolean popBackStack();
}
